package com.fkepl.my.easyui.common.support;

import com.alibaba.fastjson.JSONObject;

/**
 * ajax 通用返回结果
 * 
 * @author Fkepl
 *
 */
public class AjaxResult extends JSONObject {

	private static final long serialVersionUID = 1L;

	/**
	 * 返回错误消息
	 * 
	 * @return 错误消息
	 */
	public static AjaxResult error() {
		return message(-1, "未知错误");
	}

	/**
	 * 返回错误消息
	 * 
	 * @param errmsg 内容
	 * @return 错误消息
	 */
	public static AjaxResult error(String errmsg) {
		return error(500, errmsg);
	}

	/**
	 * 返回错误消息
	 * 
	 * @param errcode 错误码
	 * @param errmsg  内容
	 * @return 错误消息
	 */
	public static AjaxResult error(int errcode, String errmsg) {
		return message(errcode, errmsg);
	}

	/**
	 * 返回成功消息
	 * 
	 * @return 成功消息
	 */
	public static AjaxResult success() {
		return message(0, "ok");
	}

	/**
	 * 返回成功消息
	 * 
	 * @param data 数据
	 * @return 成功消息
	 */
	public static AjaxResult success(JSONObject data) {
		return message(0, "ok", data);
	}

	/**
	 * 返回消息
	 * 
	 * @param errcode 错误码
	 * @param errmsg  内容
	 * @return 消息
	 */
	public static AjaxResult message(int errcode, String errmsg) {
		AjaxResult result = new AjaxResult();
		result.put("errcode", errcode);
		result.put("errmsg", errmsg);
		return result;
	}

	/**
	 * 返回消息
	 * 
	 * @param errcode 错误码
	 * @param errmsg  内容
	 * @param data    数据
	 * @return 消息
	 */
	public static AjaxResult message(int errcode, String errmsg, JSONObject data) {
		AjaxResult result = new AjaxResult();
		result.put("errcode", errcode);
		result.put("errmsg", errmsg);
		result.put("data", data);
		return result;
	}

}