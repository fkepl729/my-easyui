package com.fkepl.my.easyui.common.support;

import java.util.List;

import org.springframework.data.domain.Page;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fkepl.my.easyui.common.util.Utils;

public class DataGrid extends JSONObject {

	private static final long serialVersionUID = 1L;

	public static DataGrid fill(List<?> rows) {
		return fill(rows, null);
	}

	public static DataGrid fill(List<?> rows, List<?> footer) {
		DataGrid grid = new DataGrid();
		grid.put("total", rows.size());
		grid.put("rows", JSONArray.parseArray(JSON.toJSONString(rows)));
		if (Utils.isNotNull(footer)) {
			grid.put("footer", JSONArray.parseArray(JSON.toJSONString(footer)));
		}
		return grid;
	}

	public static DataGrid fill(Page<?> pages) {
		return fill(pages, null);
	}

	public static DataGrid fill(Page<?> pages, List<?> footer) {
		DataGrid grid = new DataGrid();
		grid.put("total", pages.getTotalElements());
		grid.put("rows", JSONArray.parse((JSON.toJSONString(pages.getContent()))));
		if (Utils.isNotNull(footer)) {
			grid.put("footer", JSONArray.parseArray(JSON.toJSONString(footer)));
		}
		return grid;
	}

}
