package com.fkepl.my.easyui.common.support;

import java.util.List;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class ComboTree extends JSONArray {

	private static final long serialVersionUID = 1L;

	private static final String ID = "id";

	private static final String TEXT = "text";

	private static final String CHILDREN = "children";

	private static final String IS_CHILDREN = "isChildren";

	public static ComboTree fill(List<?> list, String idField, String textField, String parentIdField) {
		ComboTree tree = new ComboTree();
		JSONObject root = new JSONObject();
		root.put(ID, 0);
		root.put(TEXT, "Root");
		JSONArray array = JSONArray.parseArray(JSON.toJSONString(list));
		if (array != null && !array.isEmpty()) {
			JSONArray children = getNodes(array, 0, idField, textField, parentIdField);
			if (children != null && !children.isEmpty()) {
				root.put(CHILDREN, children);
			}
		}
		tree.add(root);
		return tree;
	}

	private static JSONArray getNodes(JSONArray array, int id, String idField, String textField, String parentIdField) {
		JSONArray nodes = new JSONArray();
		for (Object object : array) {
			JSONObject item = (JSONObject) object;
			if (!item.containsKey(IS_CHILDREN) && item.getIntValue(idField) != id) {
				if (item.getIntValue(parentIdField) == id) {
					JSONObject node = new JSONObject();

					/* 获取子节点 */
					JSONArray children = getNodes(array, item.getIntValue(idField), idField, textField, parentIdField);
					if (children != null && !children.isEmpty()) {
						node.put(CHILDREN, children);
					}

					/* 添加属性 */
					node.put(ID, item.getIntValue(idField));
					node.put(TEXT, item.getString(textField));

					nodes.add(node);
				}
			}
		}
		return nodes;
	}
}
