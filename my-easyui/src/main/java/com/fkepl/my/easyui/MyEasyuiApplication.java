package com.fkepl.my.easyui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyEasyuiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyEasyuiApplication.class, args);
	}

}

