package com.fkepl.my.easyui.controller.datagrid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fkepl.my.easyui.common.support.AjaxResult;
import com.fkepl.my.easyui.common.support.DataGrid;
import com.fkepl.my.easyui.dao.UserDao;
import com.fkepl.my.easyui.model.User;

@Controller
@RequestMapping("/datagrid")
public class DataGridController {

	@Autowired
	private UserDao dao;

	@RequestMapping("/")
	public String dataGrid() {
		return "datagrid";
	}

	@RequestMapping("/datagrid")
	@ResponseBody
	public DataGrid dataGrid(int page, int rows) {
		Page<User> pages = dao.findAll(PageRequest.of(page - 1, rows));
		return DataGrid.fill(pages);
	}

	@RequestMapping("/save")
	@ResponseBody
	public AjaxResult save(User user) {
		dao.save(user);
		return AjaxResult.success();
	}

	@RequestMapping("/delete")
	@ResponseBody
	public AjaxResult delete(@RequestParam(value = "ids[]") int[] ids) {
		for (int id : ids) {
			dao.deleteById(id);
		}
		return AjaxResult.success();
	}
}
