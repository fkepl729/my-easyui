package com.fkepl.my.easyui.common.util;

import java.util.Optional;

/**
 * 通用方法
 * 
 * @author Fkepl
 *
 */
public class Utils {

	/**
	 * * 判断一个对象是否为空
	 * 
	 * @param object 对象
	 * @return true：为空，false：非空
	 */
	public static boolean isNull(Object object) {
		return null == object;
	}

	/**
	 * * 判断一个对象是否非空
	 * 
	 * @param object 对象
	 * @return true：非空，false：为空
	 */
	public static boolean isNotNull(Object object) {
		return !isNull(object);
	}

	/**
	 * * 判断一个字符串是否为空串
	 * 
	 * @param string 字符串
	 * @return true：字符串为空，false：非空字符串
	 */
	public static boolean isEmpty(String string) {
		return isNull(string) || string.trim().isEmpty();
	}

	/**
	 * * 判断一个字符串是否为非空串
	 * 
	 * @param string 字符串
	 * @return true：非空字符串，false：字符串为空
	 */
	public static boolean isNotEmpty(String string) {
		return !isEmpty(string);
	}

	/**
	 * @param optional
	 * @return
	 */
	public static boolean isNotEmpty(Optional<?> optional) {
		if (isNotNull(optional) && optional.isPresent()) {
			return true;
		}
		return false;
	}
}