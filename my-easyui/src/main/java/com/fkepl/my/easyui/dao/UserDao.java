package com.fkepl.my.easyui.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.fkepl.my.easyui.model.User;

public interface UserDao extends JpaRepository<User, Integer>, JpaSpecificationExecutor<User> {

}
