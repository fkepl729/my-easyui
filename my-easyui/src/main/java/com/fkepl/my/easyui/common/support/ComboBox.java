package com.fkepl.my.easyui.common.support;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public class ComboBox extends JSONArray {

	private static final long serialVersionUID = 1L;

	private static final String VALUE = "value";

	private static final String TEXT = "text";

	public static ComboBox fill(List<?> list) {
		ComboBox box = new ComboBox();
		box.addAll(JSONArray.parseArray(JSON.toJSONString(list)));
		return box;
	}

	public static ComboBox fill(List<?> list, String valueField, String textField) {
		ComboBox box = new ComboBox();
		JSONArray array = JSONArray.parseArray(JSON.toJSONString(list));
		if (array != null && !array.isEmpty()) {
			for (Object object : array) {
				JSONObject item = (JSONObject) object;
				item.put(VALUE, item.get(valueField));
				item.put(TEXT, item.get(textField));
				box.add(item);
			}
		}
		return box;
	}

	public static ComboBox fillByDic(List<?> list) {
		return fill(list, "dicValue", "dicName");
	}

}
