package com.fkepl.my.easyui.common.support;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.fkepl.my.easyui.common.util.Utils;

public class TreeGrid extends JSONObject {

	private static final long serialVersionUID = 1L;

	public static TreeGrid fill(List<?> rows, String parentIdField) {
		return fill(rows, null, parentIdField);
	}

	public static TreeGrid fill(List<?> rows, List<?> footer, String parentIdField) {
		JSONArray array = JSONArray.parseArray(JSON.toJSONString(rows));
		if (array != null && !array.isEmpty()) {
			for (Object object : array) {
				JSONObject temp = (JSONObject) object;
				int parentId = temp.getIntValue(parentIdField);
				if (parentId > 0) {
					temp.put("_parentId", temp.get(parentIdField));
				}
			}
		}
		TreeGrid grid = new TreeGrid();
		grid.put("total", array.size());
		grid.put("rows", array);
		if (Utils.isNotNull(footer)) {
			grid.put("footer", JSONArray.parseArray(JSON.toJSONString(footer)));
		}
		return grid;
	}
}
