(function($) {
	var _datagrid;
	var _searchbar;
	var _form;

	$.extend($.my.easyui, {
		datagrid : function(options, param) {
			if (typeof options === 'string') {
				return $.my.easyui.datagrid.methods[options](this, param);
			}
			/* 初始化 */
			options = $.extend({}, $.my.easyui.datagrid.defaults, options);
			$.each(options.columns[0], function(i, col) {
				if (col.checkbox === undefined || col.checkbox === false) {
					col = $.extend(true, {}, $.my.easyui.datagrid.columns.defaults, col);
					if (!col.title) {
						col.title = col.field;
					}
					options.columns[0][i] = col;
				}
			});
			var firstColumns = options.columns[0][0];
			console.log(firstColumns);
			if (options.firstChecked) {
				if (firstColumns.checkbox === undefined || firstColumns.checkbox === false) {
					firstColumns = {
						field : 'ck',
						checkbox : true
					}
					options.columns[0].unshift(firstColumns);
				}
			}
			console.log(firstColumns);
			if (!options.idField) {
				if (firstColumns) {
					if (firstColumns.checkbox === undefined || firstColumns.checkbox === false) {
						options.idField = firstColumns.field;
					} else {
						options.idField = options.columns[0][1].field;
					}
				}
			}

			$.my.easyui.datagrid.options = options;

			if (options.toolbar) {
				$.my.easyui.datagrid.toolbar();
			}
			$.my.easyui.datagrid.dialog();
			$.my.easyui.datagrid.menu();
			_datagrid = $('<div class="my-datagrid"></div>').appendTo(options.parent);
			_datagrid.datagrid(options);

		}
	});

	$.extend($.my.easyui.datagrid, {
		methods : {},
		options : {},
		defaults : {
			url : './datagrid',
			method : 'get',
			toolbar : '.my-toolbar',
			emptyMsg : '无记录',
			columns : [ [] ],
			frozenColumns : undefined,
			fit : true,
			fitColumns : true,
			border : false,
			striped : true,
			idField : null,
			rownumbers : true,
			pagination : true,
			pageNumber : 1,
			pageSize : 10,
			pageList : [ 10, 30, 50, 100 ],
			onRowContextMenu : function(e, index, row) {
				if (row != null) {
					e.preventDefault();
					_datagrid.datagrid('unselectAll');
					_datagrid.datagrid('selectRow', index);
					_menu.menu('show', {
						left : e.pageX,
						top : e.pageY
					});
				}
			},
			/* 自定义 */
			parent : 'body',
			urlSave : './save',
			urlDelete : './delete',
			firstChecked : true,
		},
		columns : {
			defaults : {
				field : undefined,
				title : undefined,
				value : undefined,
				width : 100,
				type : 'textbox',
			}
		},
		toolbar : function(options, param) {
			if (typeof options === 'string') {
				return $.my.easyui.datagrid.toolbar.methods[options](this, param);
			}
			options = $.extend({}, $.my.easyui.datagrid.toolbar.defaults, options);
			_toolbar = $('<div class="my-toolbar"></div>').appendTo(options.parent);
			/* 添加查询栏 */
			if ($(options.searchbar).length > 0) {
				_searchbar = $('<div class="my-toolbar__search"></div>').appendTo(_toolbar);
				_searchbar.append($(options.searchbar));
			}
			/* 添加操作栏 */
			if (options.btnAdd || options.btnEdit || options.btnDelete || options.btnReload || (options.btnSearch && $(options.searchbar).length > 0)) {
				var _oper = $('<div class="my-toolbar__oper"></div>').appendTo(_toolbar);
				var _operLeft = $('<div class="my-toolbar__oper-left"></div>').appendTo(_oper);
				if (options.btnAdd) {
					_operLeft.append('<a class="oper-add" href="javascript:;">添加</a>');
					$('.oper-add').linkbutton({
						iconCls : 'icon-add',
						onClick : options.onAdd
					});
				}
				if (options.btnEdit) {
					_operLeft.append('<a class="oper-edit" href="javascript:;">编辑</a>');
					$('.oper-edit').linkbutton({
						iconCls : 'icon-edit',
						onClick : options.onEdit
					});
				}
				if (options.btnDelete) {
					_operLeft.append('<a class="oper-delete" href="javascript:;">删除</a>');
					$('.oper-delete').linkbutton({
						iconCls : 'icon-delete',
						onClick : options.onDelete
					});
				}
				var _operRight = $('<div class="my-toolbar__oper-right"></div>').appendTo(_oper);
				if (options.btnReload) {
					_operRight.append('<a class="oper-reload" href="javascript:;" title="刷新"></a>');
					$('.oper-reload').linkbutton({
						iconCls : 'icon-reload',
						onClick : options.onReload
					});
				}
				if (options.btnSearch && $(options.searchbar).length > 0) {
					_operRight.append('<a class="oper-search" href="javascript:;" title="查询"></a>');
					$('.oper-search').linkbutton({
						iconCls : 'icon-search',
						onClick : options.onSearch
					});
				}
			}
		},
		dialog : function(options, param) {
			if (typeof options === 'string') {
				return $.my.easyui.datagrid.dialog.methods[options](this, param);
			}
			options = $.extend({}, $.my.easyui.datagrid.dialog.defaults, options);

			_dialog = $('<div class="my-dialog"></div>').appendTo(options.parent);
			_dialog.dialog(options);

			_form = $('<form class="my-dialog__form"></form>').appendTo(_dialog);
			$.each($.my.easyui.datagrid.options.columns[0], function(i, col) {
				if (col.title) {
					if (col.type === 'hidden') {
						_form.append('<input id="' + col.field + '" name="' + col.field + '" type="hidden">');
					} else {
						_form.append('<div class="my-dialog__form-item"><span class="my-dialog__form-item__title">' + col.title + '</span><input id="' + col.field + '" name="' + col.field + '"></div>');
						var _input = $('#' + col.field);
						if (col.type === 'passwordbox') {
							_input.passwordbox($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'maskedbox') {
							_input.maskedbox($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'combobox') {
							_input.combobox($.extend({
								panelHeight : 'auto',
								panelMaxHeight : 200,
								method : 'get'
							}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'combotree') {
							_input.combotree($.extend({
								panelHeight : 'auto',
								panelMaxHeight : 200,
								method : 'get'
							}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'tagbox') {
							_input.tagbox($.extend({
								panelHeight : 'auto',
								panelMaxHeight : 200,
								method : 'get'
							}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
								hasDownArrow : col.url ? col.hasDownArrow : false,
							}));
						} else if (col.type === 'numberbox') {
							_input.numberbox($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'datebox') {
							_input.datebox($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'datetimebox') {
							_input.datetimebox($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'filebox') {
							_input.filebox($.extend({
								buttonText : '选择文件',
							}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'checkbox') {
							_input.checkbox($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'radiobutton') {
							_input.radiobutton($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
							}));
						} else if (col.type === 'switchbutton') {
							_input.switchbutton($.extend({
								onText : '是',
								offText : '否',
							}, col, {
								width : col.btnWidth ? col.btnWidth : 80,
							}));
						} else {
							_input.textbox($.extend({}, col, {
								width : col.inputWidth == undefined ? 300 : col.inputWidth,
								height : col.multiline == undefined ? null : (col.height == undefined ? col.height : 100),
							}));
						}
					}
				}
			});

		},
		menu : function(options, param) {
			if (typeof options === 'string') {
				return $.my.easyui.datagrid.menu.methods[options](this, param);
			}
			_menu = $('<div class="my-context__menu"></div>').appendTo('body');
			_menu.menu();
			_menu.menu('appendItem', {
				text : '编辑',
				onclick : function() {
					$.my.easyui.datagrid.toolbar('edit');
				}
			});
			_menu.menu('appendItem', {
				text : '删除',
				onclick : function() {
					$.my.easyui.datagrid.toolbar('del');
				}
			});
		},
	});

	$.extend($.my.easyui.datagrid.toolbar, {
		methods : {
			add : function() {
				$.each($.my.easyui.datagrid.options.columns[0], function(i, col) {
					if (col.title) {
						$.my.easyui.datagrid.dialog('setValue', col);
					}
				});
				_dialog.dialog('open');
			},
			edit : function() {
				var row = _datagrid.datagrid('getSelected');
				if (!row) {
					$.messager.alert('提示', '没有选择任何对象！', 'warning');
				} else {
					$.each($.my.easyui.datagrid.options.columns[0], function(i, col) {
						var value = row[col.field];
						if (value) {
							$.my.easyui.datagrid.dialog('setValue', {
								field : col.field,
								type : col.type,
								value : value
							});
						}
					});
					_dialog.dialog('open');
				}
			},
			del : function() {
				var rows = _datagrid.datagrid('getSelections');
				var ids = [];
				$.each(rows, function(i, row) {
					ids.push(row[$.my.easyui.datagrid.options.idField]);
				});
				if (ids.length < 1) {
					$.messager.alert('提示', '没有选择任何对象！', 'warning');
				} else {
					$.messager.confirm('提示', '是否删除所选对象？', function(r) {
						if (r) {
							$.post($.my.easyui.datagrid.options.urlDelete, {
								ids : ids
							}, function(res) {
								_datagrid.datagrid('reload');
							}).error(function() {
								$.messager.alert('提示', '提交失败！', 'error');
							});
						}
					});
				}
			},
			reload : function() {
				_datagrid.datagrid('reload');
			},
			search : function() {
				_searchbar.toggle();
				_datagrid.datagrid('resize');
			}

		},
		defaults : {
			parent : 'body',
			searchbar : '.my-toolbar__searchbar',
			btnAdd : true,
			btnEdit : true,
			btnDelete : true,
			btnReload : true,
			btnSearch : true,
			onAdd : function() {
				$.my.easyui.datagrid.toolbar('add');
			},
			onEdit : function() {
				$.my.easyui.datagrid.toolbar('edit');
			},
			onDelete : function() {
				$.my.easyui.datagrid.toolbar('del');
			},
			onReload : function() {
				$.my.easyui.datagrid.toolbar('reload');
			},
			onSearch : function() {
				$.my.easyui.datagrid.toolbar('search');
			},
		},
	});

	$.extend($.my.easyui.datagrid.dialog, {
		methods : {
			setValue : function(easyui, param) {
				var _input = $('#' + param.field);
				if (param.type === 'hidden') {
					_input.val(param.value);
				} else if (param.type === 'passwordbox') {
					_input.passwordbox('setValue', param.value ? param.value : '');
				} else if (param.type === 'maskedbox') {
					_input.maskedbox('setValue', param.value);
				} else if (param.type === 'combobox') {
					_input.combobox('setValue', param.value);
				} else if (param.type === 'combotree') {
					_input.combotree('setValue', param.value);
				} else if (param.type === 'tagbox') {
					_input.tagbox('setValue', param.value);
				} else if (param.type === 'numberbox') {
					_input.numberbox('setValue', param.value ? param.value : 0);
				} else if (param.type === 'datebox') {
					_input.datebox('setValue', param.value);
				} else if (param.type === 'datetimebox') {
					_input.datetimebox('setValue', param.value);
				} else if (param.type === 'filebox') {
					_input.filebox('setValue', param.value);
				} else if (param.type === 'checkbox') {
					_input.checkbox('setValue', param.value);
				} else if (param.type === 'radiobutton') {
					_input.radiobutton('setValue', param.value);
				} else if (param.type === 'switchbutton') {
					_input.switchbutton(param.value ? 'check' : 'uncheck');
				} else {
					_input.textbox('setValue', param.value ? param.value : '');
				}
			},
		},
		defaults : {
			parent : 'body',
			title : '新建 / 编辑',
			iconCls : 'icon-save',
			width : 640,
			height : 480,
			maximizable : true,
			resizable : true,
			closed : true,
			modal : true,
			buttons : [ {
				text : '保存',
				handler : function() {
					var url = $.my.easyui.datagrid.options.urlSave;
					$.post(url, _form.serialize(), function(res) {
						_dialog.dialog('close');
						_datagrid.datagrid('reload');
					}).error(function() {
						$.messager.alert('提示', '提交失败！', 'error');
					});
				},
			}, {
				text : '取消',
				handler : function() {
					_dialog.dialog('close');
				},
			} ],
		},
	});

	$.extend($.my.easyui.datagrid.menu, {
		methods : {},
		defaults : {}
	});
})(jQuery);
