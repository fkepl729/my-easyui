(function($) {
	$.extend({
		my : {
			easyui : {}
		}
	});

	function formatStatus(value, row, index) {
		if (value) {
			return '正常';
		} else {
			return '异常';
		}
	}

})(jQuery);
