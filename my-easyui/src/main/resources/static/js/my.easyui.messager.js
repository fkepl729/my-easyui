(function($) {
	$.extend($.my.easyui, {
		messager : function(options, param) {
			if (typeof options === 'string') {
				var methods = $.my.easyui.messager.methods[options];
				if (methods != undefined) {
					return methods(param);
				} else {
					options = {
						msg : options
					}
				}
			}
			/* 初始化 */
			options = $.extend({}, $.my.easyui.messager.defaults, options);
			if (options.msg.length > 0 || options.icon) {
				_messager = $('<div class="my-messager"></div>').appendTo('body');
				if (!options.mask) {
					_messager.addClass('my-messager__mask');
				}
				var _body = $('<div class="my-messager__body"></div>').appendTo(_messager);
				if (options.icon) {
					_body.append('<i class="fa fa-spinner fa-spin" aria-hidden="true"></i>');
				}
				_body.append('<span>' + options.msg + '</span>');
				// 自动关闭
				if (options.timeout > 0) {
					var interval = window.setInterval(function() {
						$.my.easyui.messager.closed();
						clearInterval(interval);
					}, options.timeout);
				}
			}
		}
	});

	$.extend($.my.easyui.messager, {
		methods : {
			loading : function() {
				$.my.easyui.messager.loading();
			},
			success : function() {
				$.my.easyui.messager.success();
			},
			fial : function(param) {
				$.my.easyui.messager.fial(param);
			},
			closed : function() {
				$.my.easyui.messager.closed();
			}
		},
		defaults : {
			msg : '正在处理，请稍后 ...',
			icon : true,
			timeout : 0,
			mask : true
		},
		loading : function() {
			$.my.easyui.messager({
				msg : '玩命加载中，请稍后 ...',
			});
		},
		success : function() {
			$.my.easyui.messager({
				msg : '操作成功',
				icon : false,
				timeout : 1500,
				mask : false,
			});
		},
		fial : function(reason) {
			$.my.easyui.messager({
				msg : '操作失败' + (reason ? ('，原因：' + reason) : ''),
				icon : false,
				timeout : 3000,
				mask : false,
			});
		},
		closed : function() {
			_messager.remove();
			_messager = undefined;
		},
	});
})(jQuery);
