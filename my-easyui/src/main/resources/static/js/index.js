$(function() {
	// $.my.easyui.toolbar();

	$.my.easyui.datagrid({
		url : './json/datagrid_data1.json',
		columns : [ [ {
			field : 'ck',
			checkbox : true
		}, {
			field : 'itemid',
			type : 'numberbox'
		}, {
			field : 'productid',
		}, {
			field : 'productname',
			type : 'combobox',
			url: './json/combobox_data1.json'
		}, {
			field : 'unitcost',
		}, {
			field : 'status',
		}, {
			field : 'listprice',
		}, {
			field : 'attr1',
		} ] ]
	});
});
